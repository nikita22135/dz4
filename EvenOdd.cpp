﻿#include <iostream>
enum Parity
{
    EVEN,
    ODD
};

void printNumbers(const bool parity, int quantity)
{
    for (int i = parity; i <= quantity ; i+=2)
    {
        std::cout << i << "\n";
    }
}

int main()
{
    setlocale(LC_ALL, "ru");
    const int N = 14;

    for (int i = 0; i <= N; i+=2)
    {
        std::cout << i << "\n";
    }

    if (N > 0)
    {
        std::cout << "Четные числа\n";
        printNumbers(EVEN, N);
        std::cout << "Нечетные числа\n";
        printNumbers(ODD, N);
    }
    return 0;
}

